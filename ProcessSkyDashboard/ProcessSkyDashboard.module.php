<?php
class ProcessSkyDashboard extends Process {
  public function ___execute() {
    $form = $this->modules->get('InputfieldForm');
    $form->action = './addskyscraper';

    $field = $this->modules->get('InputfieldMarkup');
    $field->id = 'table';
    $field->label = 'Table';
    $field->value = $this->renderTable();
    $field->columnWidth = 50;
    $form->add($field);

    $field = $this->modules->get('InputfieldMarkup');
    $field->label = 'Chart';
    $field->value = $this->renderChart();
    $field->columnWidth = 50;
    $form->add($field);
    
    $fieldset = $this->modules->get('InputfieldFieldset');
    $fieldset->label = 'Add Skyscraper';
    //$fieldset->collapsed = Inputfield::collapsedYes;

      $field = $this->modules->get('InputfieldText');
      $field->name = 'Name';
      $field->columnWidth = 33;
      $fieldset->add($field);

      $field = $this->modules->get('InputfieldText');
      $field->name = 'Height';
      $field->columnWidth = 33;
      $fieldset->add($field);

      // see InputfieldPage.module for all available options
      $field = $this->modules->get('InputfieldPage');
      $field->inputfield = 'InputfieldSelect';
      $field->findPagesSelector = 'template=city';
      $field->labelFieldName = 'title';
      $field->name = 'City';
      $field->columnWidth = 34;
      $fieldset->add($field);
      
      $button = $this->modules->get('InputfieldSubmit');
      $button->value = 'Save';
      $button->icon = 'floppy-o';
      $fieldset->add($button);
    
    $form->add($fieldset);

    return $form->render();
  }

  public function ___executeAddskyscraper() {
    // add page
    $p = new Page();
    $p->template = 24; // skyscraper
    $p->parent = $this->sanitizer->int($this->input->post->City);
    $p->title = $this->sanitizer->text($this->input->post->Name);
    $p->height = $this->sanitizer->float($this->input->post->Height);
    if(!$p->title) return;
    $p->save();

    // show message
    $this->message('Added a new skyscraper to the database');

    // redirect to our dashboard
    $this->session->redirect('./');
  }

  private function renderTable() {
    $out = '';
    
    $table = $this->modules->get('MarkupAdminDataTable');
    $table->id = 'skyscrapertable';
    $table->setSortable(false); // sorting is done on the client side so we disable it to prevent misunderstandings
    $table->headerRow(['Skyscraper', 'Added']);
    // $table->addClass('uk-table-striped'); // looks ugly imho but stays here to show it could be done

    // see MarkupAdminDataTable.module
    foreach($this->pages->find("template=skyscraper,limit=5,sort=-created") as $sky) {
      $table->row([$sky->title => $sky->editUrl, date("d.m.Y H:i:s", $sky->created)]);
    }
    
    $out .= $table->render();

    $button = $this->modules->get('InputfieldButton');
    $button->value = 'Add new Skyscraper';
    $button->icon = 'plus';
    $button->setSmall(true);
    $button->addClass('ui-priority-secondary pw-panel');
    $button->attr('data-href', '/processwire/page/add/?template_id=24');
    $out .= $button->render();

    return $out;
  }

  private function renderChart() {
    $out = '';

    // get number of skyscrapers
    $counts = [];
    $counts[] = $this->pages->count('template=skyscraper,height<=50');
    $counts[] = $this->pages->count('template=skyscraper,height>50,height<=150');
    $counts[] = $this->pages->count('template=skyscraper,height>150');
    $counts = implode(',', $counts);
    
    // this method works only on non-ajax fields!
    // when the field is loaded via ajax the scripts would not get injected because the page header and script-tags have already been rendered
    $this->config->scripts->add('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js');
    $this->config->scripts->add($this->config->urls->siteModules . $this->className() . '/skyscraperchart.js');
    $out = "<canvas id='chart' data-counts='$counts'></canvas>";

    return $out;
  }
}