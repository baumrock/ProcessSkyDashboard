<?php
$info = [
  'title' => 'Skyscraper Dashboard',
  'summary' => 'Demo Dashboard for my guest blogpost',
  'author' => 'Bernhard Baumrock, baumrock.com',
  'version' => 1,
  'page' => [
    'name' => 'dashboard',
    'title' => 'Dashboard',
  ],
];