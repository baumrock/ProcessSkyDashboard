// chart adopted from http://www.chartjs.org/docs/latest/
$(document).ready(function() {
  var counts = $('#chart').data('counts').split(',');
  var ctx = document.getElementById("chart").getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: ["0-50", "50-150", "150+"],
          datasets: [{
              label: 'Number of Skyscrapers with given height',
              data: counts,
              backgroundColor: 'rgba(255, 99, 132, 0.2)',
              borderColor: 'rgba(255,99,132,1)',
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
});